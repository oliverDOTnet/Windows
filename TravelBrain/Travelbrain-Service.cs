﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelBrain
{
    public class BrainFact
    {
        public int index { get; set; }
        public long id { get; set; }
        public string category { get; set; }
        public bool isQuestion { get; set; }
        public List<Answer> answers { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public string formattedDistance { get; set; }
        public string url { get; set; }
        public string picture { get; set; }
    }

    public class Answer
    {
        public string text { get; set; }
        public bool isCorrect { get; set; }
    }

}
