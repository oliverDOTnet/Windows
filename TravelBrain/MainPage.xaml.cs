﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TravelBrain
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            string location = "50.929066,11.585212";

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(TimeSpan.FromMinutes(5), TimeSpan.FromSeconds(10));

                location = geoposition.Coordinate.Point.Position.Latitude.ToString("0.000000")
                    + "," + geoposition.Coordinate.Point.Position.Longitude.ToString("0.000000");
            }
            catch (Exception ex)
            {

            }

            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(new Uri("http://travelbrain.cloudapp.net/v1/brainfacts?location=" + location));
            List<BrainFact> brainFactCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BrainFact>>(result);

            this.outputStream.ItemsSource = brainFactCollection;
        }
    }
}
